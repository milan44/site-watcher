const fs = require('fs');
const puppeteer = require('puppeteer');
const filenamifyUrl = require('filenamify-url');
const dateFormat = require('dateformat');

process.argv.shift();
process.argv.shift();

const link = process.argv.length > 0 ? process.argv[0] : null;

if (!link) {
    console.log('Usage: node main.js [link]');
    process.exit();
}

let lastStatus = '200';
let counter = 0;
async function takeScreenshot(url) {
    const file = './exports/' + filenamifyUrl(url).split('!').join('_') + '.png';

    if (!fs.existsSync('exports')) {
        fs.mkdirSync('exports');
    }

    const browser = await puppeteer.launch({
        defaultViewport: {
            width: 1920,
            height: 1080
        }
    });
    const page = await browser.newPage();
    const response = await page.goto(url, {
        waitUntil: 'networkidle0'
    });
    const headers = response.headers();
    if (headers.status !== '200') {
        if (lastStatus !== '200') {
            log('Received non 200 http response twice');
            process.exit(0);
        }
        lastStatus = headers.status;
        log('Encountered non 200 http response ('+headers.status+')');
        return;
    }

    await page.evaluate(function() {
        var ads = document.querySelector('.adg-rects.desktop');
        if (ads) {
            ads.style.display = 'none';
        }

        var images = document.querySelectorAll('a.fileThumb');
        for(var x=0;x<images.length;x++) {
            var image = images[x];
            var img = image.querySelector('img');
            if (img) {
                img.src = image.href;
            }
        }
    });

    await page.waitFor(500);

    await page.screenshot({ path: file, fullPage: true });
    await browser.close();

    counter++;
    log('Successfully took a screenshot ('+counter+')');
}

function log(msg) {
    console.log(dateFormat(new Date(), "[dd.mm.yyyy - hh:MM:ss]") + ' ' + msg);
}

async function watch(url) {
    await takeScreenshot(url);
    setTimeout(watch, 10000, url);
}

watch(link).then(iDontCare => {});